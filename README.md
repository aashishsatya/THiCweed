### THiCweed

THiCweed is a new approach to motif finding by modeling the problem as one of clustering bound regions based on sequence similarity.

## Note: 

The actual web page for the program, along with details of usage, can be found [here](https://www.imsc.res.in/~rsidd/thicweed/).
