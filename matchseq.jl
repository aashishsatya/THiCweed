#!/usr/bin/env julia
# Written by Rahul Siddharthan <rsidd@imsc.res.in>, (c) 2017.
# Licence: 2-clause BSD-like.  See http://www.imsc.res.in/~rsidd/thicweed

# this takes a cluster file, and a fasta file of new sequences, and
# for each fasta sequence and each architecture it prints the best seqLikelihood

include("thicweed_lib.jl")


# parse command line arguments    
using ArgParse;
function parse_cmdline()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--shift","-s"
        help = "do NOT consider shifted versions of sequences (default: consider shifts; if disabled also disables revcomp)"
        action = :store_false
        "--revcomp","-R"
        help = "do NOT consider reverse complements of input sequences (default: consider them if using shifts, not otherwise)"
        action = :store_false
        "filename1"
        help = "sequences to be analysed, in fasta format"
        required = true
        "filename2"
        help = "sequences that were originally clustered, in fasta format"
        required = true
        "archfile"
        help = "architectures file to be matched with sequences in fasta file"
        required = true
    end
    return parse_args(s)
end


function main()
    filename1=""
    filename2 = ""
    archfile = ""
    bestn = 3
        
    glob = Params(0.0, 0, 0.0, 0.0, 0, 0, 0,true,true,true,false,0.4)    
    glob.randlim = 0.66
    glob.minclustsize= 20
    glob.infthres = -1.0
    glob.pscount = 0.5
    glob.maxnclust = 25
    glob.maxsplitclusters = 500
    glob.use_revcomp = true
    glob.use_shift = true
    glob.debug = false
    glob.wlength = -1
    
    parsed_args = parse_cmdline()
    for (arg,val) in parsed_args
        if arg=="shift"
            glob.use_shift = val
        elseif arg=="revcomp"
            glob.use_revcomp = val
        elseif arg=="filename1"
            filename1=val
        elseif arg=="filename2"
            filename2=val
        elseif arg=="archfile"
            archfile=val
        end
    end
    
    aseqs = readfasta(filename1,glob)
    fseqs = readfasta(filename2,glob)
    f = open(archfile)
    archl = readlines(f)
    close(f)
    narchs = maximum([parse(Int64,split(l)[1]) for l in archl])
    glob.wlength = length(split(archl[1])[3])
    for l in archl
        if occursin("(-)",l)
            glob.use_revcomp = true
            break
        end
    end

    #println(glob)
    #println(glob.wlength)
    
    clusters_raw = SeqCluster[setupNullCluster() for n in 1:narchs]
    for n in 1:length(archl)
        ls = split(archl[n])
        narch = parse(Int64,ls[1])
        s = fseqs[n][2]
        lseq = length(s)
        seqnums = Int64[basedict[c] for c in s]
        # the shift_arr and revcomp_arr are initialized with dumb values
        seq = Seq(s,seqnums,lseq,n,0,0,false,div((glob.wlength-lseq),2),false,Int64[],Bool[])
        seq.shift = parse(Int64,split(ls[4],":")[2])
        seq.revcomp = (split(ls[4],":")[1]=="(-)")
        seq.seqindex = n
        pushCluster!(clusters_raw[narch],seq,glob)
    end


    for aseq in aseqs
        h = aseq[1]
        s = aseq[2]
        lseq = length(s)
        seqnums = Int64[basedict[c] for c in s]
        # the shift_arr and revcomp_arr are initialized with dumb values
        seq = Seq(s,seqnums,lseq,0,0,0,false,div((glob.wlength-lseq),2),false,Int64[],Bool[])
        seq.shift = 0
        seq.revcomp = false
        seq.seqindex = -1
        println(h)
        bestmatches = []
        for n in 1:length(clusters_raw)
            sc = clusters_raw[n]
            unsetMasks!(sc,glob)
            avglik = sum([seqLikelihood(s1,sc,glob) for s1 in sc.seqs])/sc.nseqs
            likout = seqLikelihoodAllShifts(seq,clusters_raw[n],glob)
            lik = likout[1]
            shift = likout[2]
            rc = likout[3]
            push!(bestmatches, (lik/avglik,lik,avglik,n,shift,rc))
        end
        sort!(bestmatches,rev=true)
        if bestn <= 0 || bestn > length(bestmatches)
            bestn = length(bestmatches)
        end
        for (lr, l, a, n, shift, rc) in bestmatches[1:bestn]
            println("Cluster ",n, "  llr ", l, "  avgllr ", a, "  ratio ", lr, "  shift ", shift, "  revcomp ", rc)
        end
    end
    
    println("Done.")
end

main()



