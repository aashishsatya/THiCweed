#!/usr/bin/python
# (C) 2017 Rahul Siddharthan
# Licence: BSD 2-clause (open source)


import math


colour_dict = {'A':"(0,128,0)", #green \
               'C':"(0,0,128)", #blue \
               'G':"(255,69,0)", #yellow-brown \
               'T':"(150,0,0)"}  #red


def print_svgheaders(canvas_size_x, canvas_size_y):
    return '''<?xml version="1.0" standalone="no"?>
        <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
        "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
        <svg width="'''+str(canvas_size_x)\
        + '''" height="'''+str(canvas_size_y) + '''" version="1.1"
        xmlns="http://www.w3.org/2000/svg">'''


def print_svgfooters():
    return "</svg>"

# each tuple specifies the x, y, fontsize, xscale to print the character
# in a 100x100 box at position 100,100.
charvals_dict = {'A': (88.5,199,135,1.09),\
                 'C': (79,196,129,1.19),\
                 'G': (83,196,129,1.14),\
                 'T': (80,199,135,1.24)}

def print_char_in_rect(c,x,y,width,height):
    raw_x, raw_y, raw_fontsize, raw_xscale = charvals_dict[c]
    raw_x = (raw_x*raw_xscale-100)/raw_xscale
    raw_y = raw_y-100
    
    xscale = width/100.0 * raw_xscale
    yscale = height/100.0

    scaled_x = x/xscale + raw_x
    scaled_y = y/yscale + raw_y
    
    return "<text x=\""+str(scaled_x)+"\" y=\""+str(scaled_y) \
          + "\" font-size=\""+str(raw_fontsize) +"\" font-weight=\"bold\""\
          + " font-family=\"Helvetica\" fill=\"rgb"+colour_dict[c]+"\" " \
          + "transform=\"scale("+str(xscale)+","+str(yscale)+")\">"+c\
          +"</text>"





def print_weightvec_at_x_y(wvec, xpos, ypos, xlen, yscale):
    # xlen is the length occupied by that column
    # yscale is the total height for 2 information bits i.e. the
    # maximum height available for a "perfect" nucleotide

    outstr = ""
    basestr = "ACGT"
    # get rid of log errors if there is a zero
    wvec = [x+0.00000001 for x in wvec]
    wvec = [float(x)/float(sum(wvec)) for x in wvec]
    infscore = (2.0 + sum([x*math.log(x,2) for x in wvec]))
    if infscore==0.0:
        return ""
    wvec = [x*infscore*yscale/2.0 for x in wvec]
    # at this point, the sum of all wvec is a maximum of yscale
    
    wveclist = [(wvec[n],basestr[n]) for n in range(4)]
    wveclist.sort()
#    wveclist.reverse()
    curr_ypos = ypos
    for n in range(4):
        curr_ypos -= wveclist[n][0]
        outstr += print_char_in_rect(wveclist[n][1],xpos,curr_ypos,xlen,wveclist[n][0])
    return outstr


def print_wm_at_x_y(wm,xpos,ypos,xlen,yscale):
    outstr = ""
    for n in range(len(wm)):
        outstr += print_weightvec_at_x_y(wm[n],xpos+xlen*n,ypos,xlen,yscale)
    return outstr

def read_one_wm(tflines,pseudocount):
    name = [l for l in tflines if l.startswith("NA")][0].split()[1]
    nums = [[(float(x)+pseudocount) for x in l.split()[1:5]] for l in tflines\
            if len(l)>2 and l[0].isdigit() and l[1].isdigit()]
            
    return (name,[[x/sum(l) for x in l] for l in nums])

def wm_from_seqlist(seqlist, pscount=0.0):
    basestr = "ACGT"
    wm = [[len([s for s in seqlist if s[m]==basestr[n]]) for n in range(4)] for m in range(len(seqlist[0]))]
    wm = [[float(x+pscount)/(sum(w)+4*pscount) for x in w] for w in wm]
    return wm


def readtransfac(filename,pseudocount=0):
    f = open(filename,'r')
    flines = [l for l in f.read().split("//") if "NA" in l]
    f.close()
    return [read_one_wm(l.splitlines(),pseudocount) for l in flines]

def readfasta(filename):
    fastalist=[]
    f = open(filename,'r')
    fl = f.read().splitlines()
    f.close()

    currhead=""
    currseq=""
    for l in fl:
        if l=="":
            continue
        if l[0]==">":
            if currhead:
                fastalist += [(currhead,currseq)]
            currhead = l
            currseq=""
            continue
        if (l[0].upper()>='A' and l[0].upper()<='Z') or l[0]=='-':
            currseq += "".join(l.split())
    return fastalist+[(currhead,currseq)]


def printhelp():
    print """
Generate sequence logo from a position weight matrix file in transfac 
format (may contain multiple PWMs), or from a set of aligned sequences 
in fasta format (one motif only).

Output is to stdout.  Redirect to a file. 

Usage: seqlogo_svg.py [-F] filename > filename.svg

If -F is provided, the file has to be a fasta-format sequence file, otherwise
it is a transfac-format PWM file.

Output is scalable vector graphics, viewable in Firefox (Google Chrome shows
glitches, other browsers not tested), editable/covertible to PDF/PNG with 
Inkscape or other vector graphics programs. 

"""
    

import sys
import getopt

fastafile = False
optlist, args = getopt.getopt(sys.argv[1:],'Fh')
try:
    for o in optlist:
        if o[0]=='-F':
            fastafile = True
        elif o[0]=="-h":
            printhelp()
            sys.exit()
    filename = args[0]        
except:
    printhelp()
    sys.exit()
    
if fastafile:
    fastaseqs = readfasta(filename)
    seqlist = [s for h,s in fastaseqs]
    wm = wm_from_seqlist(seqlist)
    name = filename.split(".")[0]
    wmlist = [(name,wm)]
else:
    wmlist=readtransfac(filename)

print print_svgheaders(20+20*max([len(wm[1]) for wm in wmlist]),300*len(wmlist))
for n in range(len(wmlist)):
    name,wm = wmlist[n]
    print "<text x=\"10\" y=\""+str(270+300*n)+"\"  font-family=\"Helvetica\" font-size=\"20\" font-weight=\"bold\" >"+name+"</text>"
    print print_wm_at_x_y(wm,10,250+300*n,20,240)

print print_svgfooters()


