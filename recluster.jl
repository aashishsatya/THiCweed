#!/usr/bin/env julia
# Written by Rahul Siddharthan <rsidd@imsc.res.in>, (c) 2017.
# Licence: 2-clause BSD-like.  See http://www.imsc.res.in/~rsidd/thicweed

include("thicweed_lib.jl")
using SpecialFunctions

# parse command line arguments    
using ArgParse;
function parse_cmdline()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--nclust","-N"
        help = "reduce to N clusters"
        arg_type = Int64
        default = -1
        "--cluststring","-c"
        help = "cluster the given clusters together.  Format: eg 1,2:3,5:6,4,7 will cluster the comma-separated groups together."
        arg_type = String
        default = ""
        "filename"
        help = "sequences to be clustered, in fasta format"
        required = true
        "archfile"
        help = "architectures file from previous clustering run"
        required = true
    end
    return parse_args(s)
end


function main()
    filename=""
    archfile = ""
    cluststr = ""
    
    glob = Params(0.0, 0, 0.0, 0.0, 0, 0, 0,true,true,true,false,0.0)    
    glob.randlim = 0.66
    glob.minclustsize= 20
    glob.infthres = -1.0
    glob.pscount = 0.5
    glob.maxnclust = 25
    glob.maxsplitclusters = 500
    glob.use_revcomp = true
    glob.use_shift = false
    glob.debug = false
    glob.wlength = -1
    
    parsed_args = parse_cmdline()
    for (arg,val) in parsed_args
        if arg=="nclust"
            glob.maxnclust = val
        elseif arg=="cluststring"
            cluststr = val
        elseif arg=="filename"
            filename=val
        elseif arg=="archfile"
            archfile=val
        end
    end

    fseqs = readfasta(filename,glob)
    f = open(archfile)
    archl = readlines(f)
    close(f)
    narchs = maximum([parse(Int64,split(l)[1]) for l in archl])
    glob.wlength = length(split(archl[1])[3])
    for l in archl
        if occursin("(-)",l)
            glob.use_revcomp = true
            break
        end
    end
    clusters_raw = SeqCluster[setupNullCluster() for n in 1:narchs]
    for n in 1:length(archl)
        ls = split(archl[n])
        narch = parse(Int64,ls[1])
        s = fseqs[n][2]
        lseq = length(s)
        seqnums = Int64[basedict[c] for c in s]
        # the shift_arr and revcomp_arr are initialized with dumb values
        seq = Seq(s,seqnums,lseq,n,0,0,false,div((glob.wlength-lseq),2),false,Int64[],Bool[])
        seq.shift = parse(Int64,split(ls[4],":")[2])
        seq.revcomp = (split(ls[4],":")[1]=="(-)")
        seq.seqindex = n
        pushCluster!(clusters_raw[narch],seq,glob)
    end

    restlist = Int64[]
    if occursin(",",cluststr)
        for mlist in split(cluststr,":")
            nclusts = [parse(Int64,x) for x in split(mlist,",")]
            sort!(nclusts)
            sc0 = clusters_raw[nclusts[1]]
            node0 = CNode(sc0)
            for nc in nclusts[2:end]
                push!(restlist,nc)
                sc1 = clusters_raw[nc]
                node1 = CNode(sc1)
                dist,shift,revcomp = cnodeLLRAllShifts(node0,node1,glob)
                print("$shift $revcomp\n")
                sc0 = mergeClustersWithShift!(sc0,sc1,shift,revcomp,glob)
                clusters_raw[nclusts[1]] = sc0
            end
        end
        sort!(restlist,rev=true)
        for nr in restlist
            sc = splice!(clusters_raw,nr)
        end
    end                       
    if glob.maxnclust==0 || length(clusters_raw)<=glob.maxnclust
        println("\nReordering clusters...")
    else
        println("\nReordering and merging clusters...")
    end
    clusters_raw1 = orderClusters(clusters_raw,glob)
    clusters = orderClusters(clusters_raw1,glob)

    
    #write architectures to file filename_archs.txt
    print("Writing ",length(clusters)," architectures to ",archfile[1:end-4],"_new.txt ...\n")
    writearchs(string(archfile[1:end-4],"_new.txt"),clusters,fseqs,glob)

    #write weight matrices to file filename_wms.tr
    println("Writing ",length(clusters)," matrices to ",archfile[1:end-4],"_new_wms.tr ... ")
    seqs = [s for (h,s) in fseqs]
    push!(clusters,setupSeqCluster(seqs,glob))
    writetransfac(string(archfile[1:end-4],"_new_wms.tr"),clusters,glob);
    println("Done.")
end

main()



