# Version 1.0, 2018-09-19
# Written by Rahul Siddharthan <rsidd@imsc.res.in>, (c) 2017-18.
# Licence: 2-clause BSD-like.  See http://www.imsc.res.in/~rsidd/thicweed

# struct containing global params
mutable struct Params
    randlim::Float64
    minclustsize::Int64
    infthres::Float64
    pscount::Float64
    maxnclust::Int64
    maxsplitclusters::Int64
    wlength::Int64
    use_revcomp::Bool
    use_shift::Bool
    debug::Bool
    hardmask::Bool
    clthres::Float64
end

function hardmasklett(c::Char)
    if islower(c)
        return "N"
    else
        return c
    end
end

function hardmask(seq::String)
    return join([hardmasklett(c) for c in seq],"")
end


#read sequence file in fasta format, return it as array of (header,seq)
function readfasta(filename::String,glob::Params)
    f = open(filename)
    fl = [chomp(l) for l in readlines(f)]
    close(f)
    fastalist = []
    currhead = ""
    currseq = ""
    for l in fl
        if (length(l)==0) || (l[1]=='#') || (l[1]==';')
            continue
        elseif l[1]=='>'
            if length(currhead)>0
                push!(fastalist,(currhead,currseq))
            end
            currhead = l
            currseq = ""
            continue
        elseif uppercase(l[1]) in "ABCDGHKMNRSTUVWY-"
            seq1 = join(split(l),"")
            if glob.hardmask
                seq1 = hardmask(seq1)
            end
            currseq = string(currseq, seq1)
        end
    end
    push!(fastalist,(currhead,currseq))
    return fastalist
end
         

function twodigitstr(n::Int64)
    if n<10
        return string("0",n)
    else
        return string(n)
    end
end

#write one weight matrix in transfac format to filehandle
function writeOneTransfac(f,name,basecounts,glob::Params)
    l = size(basecounts)[1]
    write(f,string("NA\t",name,"\nPO\tA\tC\tG\tT\n"))
    for n in 1:l
        bc = [basecounts[n,1]+glob.pscount,basecounts[n,2]+glob.pscount,
              basecounts[n,3]+glob.pscount,basecounts[n,4]+glob.pscount]
        bct = sum(bc)
        wm = join([string(x/bct) for x in bc],"\t")
        write(f,string(twodigitstr(n),"\t",wm,"\n"))
    end
    write(f,"//\n")
end

# replace N by 0.25 in basecounts, -1 is sort of "special value"
const basedict = Dict('A'=>0,'C'=>1,'G'=>2,'T'=>3, 'a'=>0, 'c'=>1, 'g'=>2, 't'=>3, 'N'=>-1, 'n'=>-1)

const rcdict = Dict('A'=>'T','T'=>'A','G'=>'C','C'=>'G','N'=>'N',
                    'a'=>'t','t'=>'a','g'=>'c','c'=>'g','n'=>'n')
# basic data structures
mutable struct  Seq
    seqchars::String
    seqnums::Array{Int64,1}
    lseq::Int64
    seqindex::Int64
    clustid::Int64 #used in output
    shift::Int64 # shift seq left or right
    revcomp::Bool # True == - strand
    oldshift::Int64 # this and the following for mergecluster, ie undoing splits
    oldrevcomp::Bool #
    shiftarr::Array{Int64,1}
    revcomparr::Array{Bool,1}
end

mutable struct SeqCluster
    seqs::Array{Seq,1}
    basecounts::Array{Float64,2} # includes pseudocount on initialization
    nseqs::Int64
    nseqsp::Float64 #(nseqs + pscount)
    allowsplit::Bool
    infs::Array{Float64,1} # information score of each column
    mask::Array{Bool,1} # 1 if infs >= infthres
end

# return a cluster with no sequences in it
# FIXME? note: pseudocount is hardcoded here as 0.5, shouldn't matter
function setupNullCluster()
    return SeqCluster(Int64[],zeros(Float64,(0,0)),0,4*0.5,
                      false,zeros(Float64,(0)),zeros(Bool,(0)))
end

const nullCluster=setupNullCluster()

function backupShiftRevcomp!(seqcl::SeqCluster)
    for seq in seqcl.seqs
        seq.oldshift = seq.shift
        seq.oldrevcomp = seq.revcomp
    end
end


# add a sequence to a cluster
function pushCluster!(seqcl::SeqCluster,seq::Seq,glob::Params)
    # this doesn't care about the sequence's shift/strand, assume that has been set already
    if seqcl.nseqs==0
        seqs = Seq[seq]
        bc = zeros(Float64,(glob.wlength,4))
        infs = zeros(Float64,glob.wlength)
        mask = zeros(Bool,glob.wlength)
        fill!(bc,glob.pscount)
        for p in 1:glob.wlength
            if seq.revcomp
                n = glob.wlength+1-p
            else
                n = p
            end
            if n+seq.shift<=0 || n+seq.shift > seq.lseq
                sn = -1
            else
                sn = seq.seqnums[n+seq.shift]
                if sn!= -1 && seq.revcomp
                    sn = 3-sn
                end
            end
            if sn == -1
                for m in 1:4
                    bc[p,m] += 0.25
                end
            else
                bc[p,sn+1]   += 1.
            end
        end
        seqcl.seqs = seqs
        seqcl.basecounts = bc
        seqcl.infs = infs
        seqcl.mask=mask
        seqcl.allowsplit = true
        seqcl.nseqs=1
        seqcl.nseqsp = 1.0+4*glob.pscount
        
        return seqcl
    else 
        #if seq.lseq != seqcl.seqs[1].lseq
        #    throw(DomainError())
        #end
        push!(seqcl.seqs,seq)
        for p in 1:glob.wlength
            if seq.revcomp
                n = glob.wlength+1-p
            else
                n = p
            end
            if n+seq.shift<=0 || n+seq.shift > seq.lseq
                sn = -1
            else
                sn = seq.seqnums[n+seq.shift]
                if sn!= -1 && seq.revcomp
                    sn = 3-sn
                end
            end
            if sn == -1
                for m in 1:4
                    seqcl.basecounts[p,m] += 0.25
                end
            else
                seqcl.basecounts[p,sn+1]+= 1
            end
        end        
        seqcl.nseqs += 1
        seqcl.nseqsp += 1.0
        seqcl.allowsplit = true
        return seqcl
    end
end

# remove a sequence from position n in the cluster, return it
# FIXME update basecounts correctly!
function spliceCluster!(seqcl::SeqCluster,n0::Int64,glob::Params)
    seq1 = splice!(seqcl.seqs,n0)
    for p in 1:glob.wlength
        if seq1.revcomp
            n = glob.wlength+1-p
        else
            n = p
        end
        if n+seq1.shift<=0 || n+seq1.shift > seq1.lseq
            sn = -1
        else
            sn = seq1.seqnums[n+seq1.shift]
            if sn!= -1 && seq1.revcomp
                sn = 3-sn
            end
        end
        if sn == -1
            for m2 in 1:4
                seqcl.basecounts[p,m2] -= 0.25
            end
        else
            seqcl.basecounts[p,sn+1] -= 1.
            #if seqcl.basecounts[p,sn+1] < 10
                #println(p," ",seqcl.basecounts[p,:], "\t")
            #end
            if seqcl.basecounts[p,sn+1] < 0.5
                println(p, " ",seqcl.basecounts[p,:])
                println(seqcl.basecounts)
                error("negative")
            end
        end
    end
    seqcl.nseqs -= 1
    seqcl.nseqsp -= 1.0
    seqcl.allowsplit = true
    return seq1
end

# remove a sequence from the end of the cluster
# FIXME update basecounts correctly!
function popCluster!(seqcl::SeqCluster,glob::Params)
    seq1 = pop!(seqcl.seqs)
    for p in 1:glob.wlength
        if seq1.revcomp
            n = glob.wlength+1-p
        else
            n = p
        end
        if n+seq1.shift<=0 || n+seq1.shift > seq1.lseq
            sn = -1
        else
            sn = seq1.seqnums[n+seq1.shift]
            if sn!= -1 && seq1.revcomp
                sn = 3-sn
            end
        end
        if sn == -1
            for m in 1:4
                seqcl.basecounts[p,m] -= 0.25
            end
        else
            seqcl.basecounts[p,sn+1] -= 1
            if seqcl.basecounts[p,sn+1] < 0.5
                println("popCluster error")
                println(seqcl.basecounts[p,:])
                println(seqcl.basecounts)
                error("negative in popcluster")
            end
        end
    end
    seqcl.nseqs -= 1
    seqcl.nseqsp -= 1.0
    seqcl.allowsplit = true
    return seq1
end


# given an array of sequences, set up a seq cluster 
function setupSeqCluster(seqlist::Array{},glob::Params)
    sc = setupNullCluster()
    for n in 1:length(seqlist)
        s = seqlist[n]
        lseq = length(s)
        seqnums = Int64[basedict[c] for c in s]
        seq1 = Seq(s,seqnums,lseq,n,0,0,false,div((glob.wlength-lseq),2),false,
                   [0 for n in 1:glob.maxsplitclusters],
                   [false for n in 1:glob.maxsplitclusters])
        pushCluster!(sc,seq1,glob)
    end
    return sc
end
        

function infscore(bc::Array{Float64,1},glob::Params)
    x = 2.0
    bct = sum(bc)
    for m=1:4
        p = bc[m]/bct
        x += p*log(p)/log(2.)
    end
    return x
end

function unsetMasks!(sc::SeqCluster,glob::Params)
    for n in 1:glob.wlength
        sc.mask[n] = true
    end
end

function initMasks!(sc::SeqCluster,glob::Params)
    if sc.nseqs == 0
        sc.infs = zeros(Float64,(0))
        sc.mask = zeros(Bool,(0))
    else
        for n in 1:glob.wlength
            sc.infs[n] = infscore(sc.basecounts[n,:],glob)
        end
        if glob.infthres < 0.0
            infthres0 = maximum(sc.infs)*0.005
        else
            infthres0 = glob.infthres
        end
        for n in 1:glob.wlength
            i1 = max(n-2,1)
            i2 = min(n+2,glob.wlength)
            sc.mask[n] = (sum(sc.infs[i1:i2])/(i2-i1+1) >= infthres0)
        end
    end    
end

# shift = how much this sequence should be shifted right or left
# rc = revcomp *before* shifting. (shift is independent of revcomp.)
# log likelihood that seq1 is sampled from the same WM as the cluster sc
function seqLikelihood(seq1::Seq,sc::SeqCluster,glob::Params)
    loglik = 0.0::Float64
    if length(sc.seqs)==0
        return loglik
    else
        nskip = 0::Int64
        for n0 in 1:glob.wlength
            if sc.mask[n0]==false
                nskip += 1
                continue
            end
            if seq1.revcomp
                n = glob.wlength+1-n0
            else
                n = n0
            end
            if n+seq1.shift <= 0 || n+seq1.shift > seq1.lseq
                sn = -1
            else
                sn = seq1.seqnums[n+seq1.shift]
            end
            if seq1.revcomp && sn!=-1
                sn = 3-sn
            end                
            if sn == -1
                loglik += log(sc.nseqsp/4.)
            else
                try
                    loglik += log(sc.basecounts[n0,sn+1])
                catch
                    println(sn," ", n0, " ", sc.basecounts[n0,:])
                    println(sc.basecounts)
                    exit()
                end
            end
        end
        loglik -= (glob.wlength-nskip)*log(sc.nseqsp/4.)                
        return loglik
    end
end     


function seqLikelihoodAllShifts(seq::Seq,sc::SeqCluster,glob::Params)
    oldshift = seq.shift
    oldrevcomp = seq.revcomp
    if glob.use_shift
        shiftrange = -div(glob.wlength,2):(seq.lseq-div(glob.wlength,2))
    else
        shiftrange = Int64[0]
    end
    if glob.use_revcomp
        rcrange = [false,true]
    else
        rcrange = [false]
    end
    bestlik,bests,bestr = -1.0e20,0,false
    for sh in shiftrange
        for rc in rcrange
            seq.shift = sh
            seq.revcomp = rc
            l = seqLikelihood(seq,sc,glob)
            if l > bestlik
                bestlik = l
                bests = sh
                bestr = rc
            end
        end
    end
    seq.shift = oldshift
    seq.revcomp = oldrevcomp
    return bestlik,bests,bestr
end
                

# log likelihood ratio of the sequences in sc1 (and sc2 if
# supplied) all being sampled from the *same* WM
function clusterLikelihood(glob::Params,sc1::SeqCluster,sc2::SeqCluster=nullCluster)
    if glob.debug
        print(sc1.basecounts)
        print(length(sc1.seqs))
        print("\n")
    end
    bc = deepcopy(sc1.basecounts)
    if sc2 != nullCluster && length(sc2.seqs)>0
        for n in 1:glob.wlength
            for m in 1:4
                @inbounds bc[n,m] += sc2.basecounts[n,m]-glob.pscount
            end
        end
    end
    ntot = sc1.nseqsp+sc2.nseqs 
    loglik = 0.0
    p4 = glob.pscount*4.0
    lpfac = lgamma(p4)-4*lgamma(glob.pscount)-lgamma(ntot)
    loglik = glob.wlength*lpfac
    for n in 1:glob.wlength
        for m in 1:4
            @inbounds loglik += lgamma(bc[n,m])
        end
    end
    return loglik
end

#FIXME consider equal breaks
function clThres(glob::Params,ns2::Int64,ns2b::Int64)
    p4 = glob.pscount*4.0
    nseqs = ns2+ns2b
    ns2 = div(nseqs,2)
    ns2b = nseqs-ns2
    ll1 = lgamma(p4)-lgamma(glob.pscount)-lgamma(ns2+p4)+lgamma(ns2+glob.pscount)
    ll1 += lgamma(p4)-lgamma(glob.pscount)-lgamma(ns2b+p4)+lgamma(ns2b+glob.pscount)
    ll1 -= lgamma(p4)-2*lgamma(glob.pscount)-lgamma(nseqs+p4)+lgamma(ns2+glob.pscount)+lgamma(ns2b+glob.pscount)
    ns2_4 = div(ns2,4)
    ns2_4r = ns2-3*ns2_4
    ns2b_4 = div(ns2b,4)
    ns2b_4r = ns2b-3*ns2b_4
    ns_4 = div(nseqs,4)
    ns_4r = nseqs-3*ns_4
    ll2 = (lgamma(p4)-4*lgamma(glob.pscount)-lgamma(ns2+p4)-
                             lgamma(ns2b+p4)+lgamma(nseqs+p4))
    ll2 += (3*lgamma(ns2_4+glob.pscount) + lgamma(ns2_4r+glob.pscount))
    ll2 += (3*lgamma(ns2b_4+glob.pscount) + lgamma(ns2b_4r+glob.pscount))
    ll2 -= (3*lgamma(ns_4+glob.pscount) + lgamma(ns_4r+glob.pscount))
    lla = ll1+(glob.wlength-1)*ll2
    llb = ll1*2+(glob.wlength-2)*ll2
    ll = lla + glob.clthres*(llb-lla)
    if glob.debug
        print("lthres's $(lla) $(llb) $(ll) \n")
    end
    return max(ll,0.0)
end


function globalShiftCluster!(sc::SeqCluster,glob::Params)
    if glob.use_shift &&  sc.nseqs>0
        unsetMasks!(sc,glob)
        shifted = true
        while shifted
            shifted = false
            backupShiftRevcomp!(sc)
            sc1 = setupNullCluster()
            ll0 = clusterLikelihood(glob,sc)
            for n in 1:sc.nseqs
                seq = popCluster!(sc,glob)
                if seq.revcomp
                    seq.shift += 1
                else
                    seq.shift -= 1
                end
                pushCluster!(sc1,seq,glob)
            end
            ll1 = clusterLikelihood(glob,sc1)
            sc2 = setupNullCluster()
            for n in 1:sc1.nseqs
                seq = popCluster!(sc1,glob)
                if seq.revcomp
                    seq.shift -= 2
                else
                    seq.shift += 2
                end
                pushCluster!(sc2,seq,glob)
            end
            ll2 = clusterLikelihood(glob,sc2)
            if ll0>=ll1 && ll0 >= ll2
                shifted=false
                sc = setupNullCluster()
                for n in 1:sc2.nseqs
                    seq = popCluster!(sc2,glob)
                    if seq.revcomp
                        seq.shift += 1
                    else
                        seq.shift -= 1
                    end
                    pushCluster!(sc,seq,glob)
                end
            elseif ll2>=ll1 && ll2 >= ll0
                shifted=true
                sc = sc2
            else
                shifted=true
                sc = setupNullCluster()
                for n in 1:sc2.nseqs
                    seq = popCluster!(sc2,glob)
                    if seq.revcomp
                        seq.shift += 2
                    else
                        seq.shift -= 2
                    end
                    pushCluster!(sc,seq,glob)
                end
            end
        end
        initMasks!(sc,glob)
    end
    return sc
end
                

function shiftOneCluster!(sc::SeqCluster,glob::Params)
    if glob.use_shift && sc.nseqs>0
        unsetMasks!(sc,glob)
        shifted = true
        nshifted = 0
        while shifted && nshifted < sc.nseqs*10
            shifted = false
            for n in 1:length(sc.seqs)
                nshifted += 1
                seq = spliceCluster!(sc,1,glob)
                oldshift = seq.shift
                oldrevcomp = seq.revcomp
                bestl,bests,bestrc = seqLikelihoodAllShifts(seq,sc,glob)
                if bests != oldshift || bestrc != oldrevcomp
                    shifted = true
                end
                seq.shift = bests
                seq.revcomp = bestrc
                pushCluster!(sc,seq,glob)
            end
        end
        initMasks!(sc,glob)
    end
end
        
        
# combine the two clusters into one (the first)
function mergeClusters!(sc1::SeqCluster,sc2::SeqCluster,glob::Params)
    # restore old shifts / revcomps before merging
    scnew = setupNullCluster()
    for n in 1:sc1.nseqs
        seq0 = spliceCluster!(sc1,1,glob)
        seq0.shift = seq0.oldshift
        seq0.revcomp = seq0.oldrevcomp
        pushCluster!(scnew,seq0,glob)
    end
    for n in 1:sc2.nseqs
        seq0 = popCluster!(sc2,glob)
        seq0.shift = seq0.oldshift
        seq0.revcomp = seq0.oldrevcomp
        pushCluster!(scnew,seq0,glob)
    end
    sc2.allowsplit=true
    sc1.allowsplit=true
    #shiftOneCluster!(sc1)
    initMasks!(scnew,glob)
    return scnew
end



# maximum-likelihood split of one cluster in two
function splitCluster_quench!(glob::Params,sc::SeqCluster,sc2::SeqCluster=nullCluster,nequilib::Int64=4)
    if sc2 == nullCluster
        backupShiftRevcomp!(sc)
        #initMasks!(sc,glob)
        nseqs = sc.nseqs
        sc2 = setupNullCluster()    
        scores = Tuple{Float64,Int64}[(seqLikelihood(sc.seqs[n],sc,glob),n)
                                      for n in 1:sc.nseqs]
        sort!(scores,rev=true)
        if nseqs<40
            nsmall = div(nseqs,2)
        elseif nseqs < 100
            nsmall = 20
        else
            nsmall = div(nseqs,5)
        end
        nlist = Int64[n for (s,n) in scores[1:nsmall]]
        sort!(nlist,rev=true)
        for n in nlist
            seq1 = spliceCluster!(sc,n,glob)
            pushCluster!(sc2,seq1,glob)
        end
    end
    initMasks!(sc,glob)
    initMasks!(sc2,glob)

    if glob.use_revcomp
        revcomprange = [false,true]
    else
        revcomprange = [false]
    end
    
    reassigned = true
    nreass=0
    while reassigned && nreass < 100
        nreass += 1
        reassigned = false
        nc = length(sc.seqs)
        nc2 = length(sc2.seqs)
        # FIXME shouldn't happen that nc or nc2 == 0
        if nc>0
            shuffle!(sc.seqs)
        end
        if nc2>0
            shuffle!(sc2.seqs)
        end
        ncrange = shuffle!(collect(1:(nc+nc2)))
        for n in ncrange
            if n<=nc
                nclust0 = 1
                seq1 = spliceCluster!(sc,1,glob)
            else
                nclust0 = 2
                seq1 = spliceCluster!(sc2,1,glob)
            end
            if nreass<=nequilib
                score1,shift1,rc1 = seqLikelihoodAllShifts(seq1,sc,glob)
                score2,shift2,rc2 = seqLikelihoodAllShifts(seq1,sc2,glob)
                seq1.shiftarr[1] = shift1
                seq1.revcomparr[1] = rc1
                seq1.shiftarr[2] = shift2
                seq1.revcomparr[2] = rc2
                if score1 > score2
                    score,nclust,shift,revcomp = score1,1,shift1,rc1
                else
                    score,nclust,shift,revcomp = score2,2,shift2,rc2
                end
                seq1.shift = shift
                seq1.revcomp = revcomp
                
            else
                seq1.shift = seq1.shiftarr[1]
                seq1.revcomp = seq1.revcomparr[1]
                score1 = seqLikelihood(seq1,sc,glob)
                seq1.shift = seq1.shiftarr[2]
                seq1.revcomp = seq1.revcomparr[2]
                score2 = seqLikelihood(seq1,sc2,glob)
                if score1 > score2
                    score,nclust = score1,1
                    seq1.shift = seq1.shiftarr[1]
                    seq1.revcomp = seq1.revcomparr[1]
                else
                    score,nclust = score2,2
                    seq1.shift = seq1.shiftarr[2]
                    seq1.revcomp = seq1.revcomparr[2]
                end
            end
            if nclust==1
                pushCluster!(sc,seq1,glob)
            else
                pushCluster!(sc2,seq1,glob)
            end
            if nclust!=nclust0
                reassigned=true
            end
        end
    end
    return sc,sc2
end


# maximum-likelihood split of one cluster in two
function splitCluster!(glob::Params,sc::SeqCluster,sc2::SeqCluster=nullCluster,nequilib::Int64=2)
    betafactor=1.05
    beta0 = 1.0
    beta = beta0
    if sc2 == nullCluster
        backupShiftRevcomp!(sc)
        # splitCluster never called directly, only via splitCluster_rand!
        # which takes care of following two lines
        #sc = globalShiftCluster!(sc,glob)
        #initMasks!(sc,glob)
        nseqs = sc.nseqs
        sc2 = setupNullCluster()    
        scores = Tuple{Float64,Int64}[(seqLikelihood(sc.seqs[n],sc,glob),n)
                                      for n in 1:sc.nseqs]
        sort!(scores,rev=true)
        if nseqs<40
            nsmall = div(nseqs,2)
        elseif nseqs < 100
            nsmall = 20
        else
            nsmall = div(nseqs,5)
        end
        nlist = Int64[n for (s,n) in scores[1:nsmall]]
        sort!(nlist,rev=true)
        for n in nlist
            seq1 = spliceCluster!(sc,n,glob)
            pushCluster!(sc2,seq1,glob)
        end
        #sc = globalShiftCluster!(sc,glob)
        #sc2 = globalShiftCluster!(sc2,glob)
    end
    initMasks!(sc,glob)
    initMasks!(sc2,glob)

    if glob.use_revcomp
        revcomprange = [false,true]
    else
        revcomprange = [false]
    end
    
    reassigned = true
    while reassigned && beta < 7.0
        beta *= betafactor
        reassigned = false
        nc = length(sc.seqs)
        nc2 = length(sc2.seqs)
        # FIXME shouldn't happen that nc or nc2 == 0
        if nc > 0
            shuffle!(sc.seqs)
        end
        if nc2>0
            shuffle!(sc2.seqs)
        end
        ncrange = shuffle!(collect(1:(nc+nc2)))
        for n in ncrange
            if n<=nc
                nclust0 = 1
                seq1 = spliceCluster!(sc,1,glob)
            else
                nclust0 = 2
                seq1 = spliceCluster!(sc2,1,glob)
            end
            if beta<=beta0*(betafactor^nequilib+1.0)
                score1,shift1,rc1 = seqLikelihoodAllShifts(seq1,sc,glob)
                score2,shift2,rc2 = seqLikelihoodAllShifts(seq1,sc2,glob)
                seq1.shiftarr[1] = shift1
                seq1.revcomparr[1] = rc1
                seq1.shiftarr[2] = shift2
                seq1.revcomparr[2] = rc2
                sadj = max(score1,score2)
                score1 = 1+score1-sadj
                score2 = 1+score2-sadj
                r = exp((score1-score2)*beta)
                r = r/(1.0+r)
                if rand() < r
                    score,nclust,shift,revcomp = score1,1,shift1,rc1
                else
                    score,nclust,shift,revcomp = score2,2,shift2,rc2
                end
                seq1.shift = shift
                seq1.revcomp = revcomp
                
            else
                seq1.shift = seq1.shiftarr[1]
                seq1.revcomp = seq1.revcomparr[1]
                score1 = seqLikelihood(seq1,sc,glob)
                seq1.shift = seq1.shiftarr[2]
                seq1.revcomp = seq1.revcomparr[2]
                score2 = seqLikelihood(seq1,sc2,glob)
                sadj = max(score1,score2)
                score1 = 1+score1-sadj
                score2 = 1+score2-sadj
                r = exp((score1-score2)*beta)
                r = r/(1.0+r)
                if rand() < r
                    score,nclust = score1,1
                    seq1.shift = seq1.shiftarr[1]
                    seq1.revcomp = seq1.revcomparr[1]
                else
                    score,nclust = score2,2
                    seq1.shift = seq1.shiftarr[2]
                    seq1.revcomp = seq1.revcomparr[2]
                end
            end
            if nclust==1
                pushCluster!(sc,seq1,glob)
            else
                pushCluster!(sc2,seq1,glob)
            end
            if nclust!=nclust0
                reassigned=true
            end
        end
    end
    return sc,sc2
end


function clustnums(cluster)
    return [Set([sc.seqs[n].seqindex for n in 1:sc.nseqs]) for sc in cluster]
end

function choose(n,m)
    if n<m
        return 0.0
    else
        return exp(lgamma(n+1) - lgamma(m+1) -lgamma(n-m+1))
    end
end

function choose2(n)
    return n*(n-1)/2
end

# adjusted rand index of two clusters
function randscore(clust1::Array{Set{Int64},1},clust2::Array{Set{Int64},1})
    ntot = sum([length(c) for c in clust1])
    l1 = length(clust1)
    l2 = length(clust2)
    nmat = zeros(Float64,(l1,l2))
    for n in 1:l1
        for m in 1:l2
            nmat[n,m] = length(intersect(clust1[n],clust2[m]))
        end
    end
    avec = zeros(Float64,l2)
    bvec = zeros(Float64,l1)
    for n in 1:l1
        bvec[n] = sum(nmat[n,:])
    end
    for n in 1:l2
        avec[n] = sum(nmat[:,n])
    end
    index = sum([choose(x,2) for x in nmat])
    maxindex = 0.5*(sum([choose(a,2) for a in avec]) + sum([choose(b,2) for b in bvec]))
    expindex = (sum([choose(a,2) for a in avec])*sum([choose(b,2) for b in bvec]))/choose(ntot,2)
    return index/maxindex, (index-expindex)/(maxindex-expindex)
end

# wallace index of two clusters
function wallacescore(clust1::Array{Set{Int64},1},clust2::Array{Set{Int64},1})
    ntot = sum([length(c) for c in clust1])
    l1 = length(clust1)
    l2 = length(clust2)
    nmat = zeros(Float64,(l1,l2))
    for n in 1:l1
        for m in 1:l2
            nmat[n,m] = length(intersect(clust1[n],clust2[m]))
        end
    end
    
    avec = zeros(Float64,l2)
    bvec = zeros(Float64,l1)
    for n in 1:l1
        bvec[n] = sum(nmat[n,:])
    end
    for n in 1:l2
        avec[n] = sum(nmat[:,n])
    end
    T = sum([choose2(x) for x in nmat])
    kh = l1*l2
    n1 = div(ntot,kh)
    r = mod(ntot,kh)
    Tmin = n1*kh/2*(n1-1) + r*n1
    T -= Tmin
    V1 = 0.5*(sum([x^2 for x in bvec])-sum([x^2 for x in nmat]))
    V2 = 0.5*(sum([x^2 for x in avec])-sum([x^2 for x in nmat]))    
    TpV1 = 0.5*(sum([x^2 for x in bvec])-ntot)
    TpV2 = 0.5*(sum([x^2 for x in avec])-ntot)
    return T/(T+V1),T/(T+V2)
end

# split one cluster, but do it three or four times.  Insist that the
# rand index > a threshold for three pairs of splits.
# Report only those sequences that are assigned consistently as
# part of the two "split" clusters, and other sequences as a third
# cluster.

# make 4 splits, so 6 rand scores.  Minimum 3 should be good, so take 3 best
# splits a, b, c, d
# rands ab, ac, ad, bc, bd, cd
# take best 3 rands that make a triangle of splits (eg, ab,ad,bd)
# If those are above threshold, do the rest


function splitCluster_rand!(sc::SeqCluster,glob::Params)    
    if !sc.allowsplit
        return sc,nullCluster,nullCluster
    end
    shiftOneCluster!(sc,glob)
    backupShiftRevcomp!(sc)
    initMasks!(sc,glob)
    sc,sc2 = splitCluster!(glob,sc)
    scna = clustnums([sc,sc2])
    llr1 =  clusterLikelihood(glob,sc) +
        clusterLikelihood(glob,sc2) -clusterLikelihood(glob,sc,sc2)
    if glob.randlim == 0.0
        if glob.clthres > 0.0
            clt = clThres(glob,sc.nseqs,sc2.nseqs)
        else
            clt = 0.0
        end
        if llr1 > clt
            return sc,sc2,nullCluster
        else
            sc = mergeClusters!(sc,sc2,glob)
            return sc,nullCluster,nullCluster
        end
    end
    sc = mergeClusters!(sc,sc2,glob)
    #shiftOneCluster!(sc,glob)
    #backupShiftRevcomp!(sc)
    sc,sc2 = splitCluster!(glob,sc)
    scnb = clustnums([sc,sc2])
    llr2 =  clusterLikelihood(glob,sc) +
        clusterLikelihood(glob,sc2) -clusterLikelihood(glob,sc,sc2)
    sc = mergeClusters!(sc,sc2,glob)
    #shiftOneCluster!(sc,glob)
    #backupShiftRevcomp!(sc)
    sc,sc2 = splitCluster!(glob,sc)
    scnc = clustnums([sc,sc2])    
    llr3 =  clusterLikelihood(glob,sc) +
        clusterLikelihood(glob,sc2) -clusterLikelihood(glob,sc,sc2)
    r1,ar1 = wallacescore(scna,scnb)
    r2,ar2 = wallacescore(scna,scnc)
    r3,ar3 = wallacescore(scnb,scnc)
    ar1 = (r1*ar1)^0.5
    ar2 = (r2*ar2)^0.5
    ar3 = (r3*ar3)^0.5
    #sc = mergeClusters!(sc,sc2,glob)
    if glob.debug
        print(ar1," ",ar2," ",ar3,"\n")
    end
    if glob.clthres > 0.0
        clt = clThres(glob,sc.nseqs,sc2.nseqs)
    else
        clt = 0.0
    end
    if glob.debug
        print("$llr1 $llr2 $llr3 $clt \n")
    end
    if max(ar1,ar2,ar3)<glob.randlim || (clt>0.0 && min(llr1,llr2,llr3) < 1.0*clt)
        sc = mergeClusters!(sc,sc2,glob)
        return sc,nullCluster,nullCluster
    elseif min(ar1,ar2,ar3)<glob.randlim
        if ar2 >=glob.randlim # a,c
            scnb = scnc
        elseif ar3>=glob.randlim # b,c
            scna = scnc
        end
        sc = mergeClusters!(sc,sc2,glob)
        sc,sc2 = splitCluster!(glob,sc)
        scnc = clustnums([sc,sc2])
        r3,ar3 = wallacescore(scnb,scnc)
        r2,ar2 = wallacescore(scna,scnc)
        ar2 = (r2*ar2)^0.5
        ar3 = (r3*ar3)^0.5
        if ar3 < glob.randlim || ar2 < glob.randlim 
            sc = mergeClusters!(sc,sc2,glob)
            return sc,nullCluster,nullCluster
        end
        if glob.debug
            print(ar2," ",ar3," ")
        end
    end
    #fallthrough from second option above
            
    if length(intersect(scnb[2],scnc[1]))>length(intersect(scnb[1],scnc[1])) &&
        length(intersect(scnb[1],scnc[2]))>length(intersect(scnb[2],scnc[2]))
        scnb = [scnb[2],scnb[1]]
    end
    if length(intersect(scnc[2],scna[1]))>length(intersect(scnc[1],scna[1])) &&
        length(intersect(scnc[1],scna[2]))>length(intersect(scnc[2],scna[2]))
        scna = [scna[2],scna[1]]
    end
    sci1 = intersect(scna[1],scnb[1],scnc[1])
    sci2 = intersect(scna[2],scnb[2],scnc[2])
    sc1n = setupNullCluster()
    sc2n = setupNullCluster()
    sc3n = setupNullCluster()
    for n in 1:sc.nseqs
        seq = popCluster!(sc,glob)
        if seq.seqindex in sci1
            pushCluster!(sc1n,seq,glob)
        else
            pushCluster!(sc3n,seq,glob)
        end
    end
    for n in 1:sc2.nseqs
        seq = popCluster!(sc2,glob)
        if seq.seqindex in sci2
            pushCluster!(sc2n,seq,glob)
        else
            pushCluster!(sc3n,seq,glob)
        end
    end
    shiftOneCluster!(sc3n,glob)
    sc1n = globalShiftCluster!(sc1n,glob)
    sc2n = globalShiftCluster!(sc2n,glob)
    sc3n = globalShiftCluster!(sc3n,glob)        
    return sc1n,sc2n,sc3n
    # =#
end
            

# given a current clustering, improve by reassigning badly-positioned sequences to other clusters.
#  strategy: 
#  at start of each iteration, identify n closest clusters to each cluster (n=Nclust/3 say, maybe
#  decreasing with each iteration)
#  also identify ns worst sequences globally (again, bottom 1/3 say, again decreasing with each iteration)
#  sample only those ns bad sequences, and only for reassignment into their n neighbouring clusters.

# sort the ns bad sequences in order of (cluster id, seq id), remove them from tail (pop!), 
# that way the ids of earlier seqs won't change. 

function reassignClusters!(sclist::Array{SeqCluster,1},glob::Params)

    totseqs = sum([length(sc.seqs) for sc in sclist])
    nsc = length(sclist)
    #if totseqs > 20*glob.minclustsize
    #    nworstseq = div(totseqs,20)
    #elseif totseqs > 10*glob.minclustsize
    #    nworstseq = div(totseqs,10)
    #else
    if totseqs > 5*glob.minclustsize
        nworstseq = div(totseqs,5)
    elseif totseqs > 2*glob.minclustsize
        nworstseq = div(totseqs,2)
    else
        nworstseq = glob.minclustsize
    end
    nbestclust = min(5,nsc-2)
    reassigned=true
    nreassigned=0
    nreass = 0
    while reassigned && nreassigned < totseqs
        if glob.debug
            print("\r$(totseqs) $(nreassigned)      ")
        end
        sclist = SeqCluster[sc for sc in sclist if sc.nseqs>0]
        for sc in sclist
            initMasks!(sc,glob)
        end        
        reassigned=false
        nsc = length(sclist)
        seqlist = []
        clustlist = [Int64[] for n in 1:nsc]
        for nclust in 1:nsc
            sc = sclist[nclust]
            for mseq in 1:sc.nseqs
                seq = sc.seqs[mseq]
                push!(seqlist,(seqLikelihood(seq,sc,glob),nclust,mseq))
            end
            lclusts = []
            for nclust2 in 1:nsc
                if nclust2==nclust
                    continue
                end
                sc2 = sclist[nclust2]
                push!(lclusts,(clusterLikelihood(glob,sc,sc2)-clusterLikelihood(glob,sc)-clusterLikelihood(glob,sc2),nclust2))
            end
            sort!(lclusts)
            reverse!(lclusts)
            for n in 1:nbestclust
                push!(clustlist[nclust],lclusts[n][2])
            end
            if sc.nseqs>=glob.minclustsize
                push!(clustlist[nclust],nclust)
            end
        end
        sort!(seqlist)
        seqlist = splice!(seqlist,1:nworstseq)
        seqlist = [(nclust,mseq) for (score,nclust,mseq) in seqlist]

        # make sure you destroy all too-small clusters
        for nclust in 1:nsc
            sc = sclist[nclust]
            if sc.nseqs < glob.minclustsize
                for mseq in 1:sc.nseqs
                    if !((nclust,mseq) in seqlist)
                        push!(seqlist,(nclust,mseq))
                    end
                end
            end
        end
        
        sort!(seqlist)
        if glob.use_revcomp
            revcomprange = [false,true]
        else
            revcomprange = [false]
        end
        
        while length(seqlist)>0
            nc,mseq = pop!(seqlist)
            sc = sclist[nc]
            seq1 = spliceCluster!(sc,mseq,glob)
            scores = Tuple{Float64,Int64,Int64,Bool}[]
            if glob.use_shift
                shiftrange = -div(glob.wlength,2):seq1.lseq-div(glob.wlength,2)
            else
                shiftrange = [0]
            end
            for nc2 in clustlist[nc]
                sc2 = sclist[nc2]
                for shift in shiftrange
                    for revcomp in revcomprange
                        seq1.shift = shift
                        seq1.revcomp = revcomp
                        push!(scores, (seqLikelihood(seq1,sc2,glob),nc2,shift,revcomp))
                    end
                end
            end
            maxs,nc2,shift,revcomp = maximum(scores)
            seq1.shift = shift
            seq1.revcomp = revcomp
            pushCluster!(sclist[nc2],seq1,glob)
            if nc2 != nc
                reassigned=true
                nreassigned += 1
                nreass += 1
                sclist[nc2].allowsplit=true
                sc.allowsplit=true
            end
        end
        if nworstseq > 10
            nworstseq = div(nworstseq*9,10)
        end
        if nbestclust > 2
            nbestclust = div(nbestclust*9,10)
        end
        sclist = filter(sc->length(sc.seqs)>0,sclist)
    end
    return sclist, nreass
end
    


# given a list of sequences, cluster them by
# 1. initialising into one big cluster
# 2. repeatedly calling splitCluster_rand!
#    on each current cluster, to make a new cluster list
# 3. After every three rounds of this, call reassignClusters
# 4. If no splits have occurred, call reassignClusters one last
#    time and return the result
function clusterSequences(seqlist::Array{String,1},glob::Params)

    if glob.minclustsize <= 0
        glob.minclustsize = div(length(seqlist),100)
    end
    clusters = SeqCluster[setupSeqCluster(seqlist,glob)]
    lseq = glob.wlength
    split=true
    splititers = 0
    while split && (glob.maxsplitclusters == 0 ||
                    length(clusters) <= glob.maxsplitclusters)
        splititers += 1
        split=false        
        newclusters = SeqCluster[]
        junkCluster = setupNullCluster()
        print(length(clusters)," clusters... ")
        for sc in clusters            
            if !sc.allowsplit
                push!(newclusters,sc)
                continue
            end
            nseqs = length(sc.seqs)
            if nseqs<glob.minclustsize
                push!(newclusters,sc)
                continue
            end
            sc,sc2,sc3 = splitCluster_rand!(sc,glob)
            if sc.nseqs==0 && sc3.nseqs==0
                push!(newclusters,sc2)
            elseif sc2.nseqs==0 && sc3.nseqs==0
                push!(newclusters,sc) 
            elseif ((sc.nseqs < glob.minclustsize) || (sc2.nseqs < glob.minclustsize))
                sc = mergeClusters!(sc,sc2,glob)
                if sc3.nseqs > 0
                    junkCluster = mergeClusters!(junkCluster,sc3,glob)
                end
                sc.allowsplit = false
                push!(newclusters,sc)
            else
                push!(newclusters,sc)
                push!(newclusters,sc2)
                if sc3.nseqs>0
                    junkCluster = mergeClusters!(junkCluster,sc3,glob)
                end
                split=true
            end
        end
        if junkCluster.nseqs > 0
            push!(newclusters,junkCluster)
        end
        if  split && length(newclusters)>2 && mod(splititers,2)==0
            print("\nReassigning among all clusters... ")
            flush(stdout)
            newclusters,nreassigned = reassignClusters!(newclusters,glob)
            println("Done. ")
            flush(stdout)                
        end
        clusters=filter(sc->length(sc.seqs)>0,newclusters)
        if length(clusters)==1
            split=false
        end
    end
    if  length(clusters)>=3 
        print("\nFinal reassigning among all clusters... ")
        flush(stdout)
        clusters, nreassigned = reassignClusters!(clusters,glob);
        println("Done.")
        flush(stdout)
    end
    sort!(clusters,by=sc->sc.nseqs,rev=true)
    return clusters
end
            
# write WMs of current clusters to a transfac file
function writetransfac(filename::String,clusters,glob::Params)
    f = open(filename,"w")
    if glob.use_shift
        nwm = length(clusters)-1
    else
        nwm = length(clusters)
    end
    for n in 1:nwm
        sc = clusters[n]
        if n<length(clusters)
            writeOneTransfac(f,string("Cluster_",n,"_(",length(sc.seqs),"seqs)"),sc.basecounts,glob)
        else
            writeOneTransfac(f,string("Full_set_(",length(sc.seqs),"seqs)"),sc.basecounts,glob)
        end
    end
    close(f)
end

# write current "architectures" to file in nplb format
function writearchs(filename,clusters,fseqs,glob::Params)
    for n in 1:length(clusters)
        for s in clusters[n].seqs
            s.clustid = n
        end
    end
    seqsn = []
    for c in clusters
        seqsn = [seqsn; [(seq.seqindex,seq) for seq in c.seqs]]
    end
    sort!(seqsn)
    f = open(filename,"w")
    for n in 1:length(seqsn)
        h,s = fseqs[n]
        if seqsn[n][1]!=n 
            print("problem -- inconsistent indexing! ")
            println("  $n  $(seqsn[n])")
            exit()
        end
        seq = seqsn[n][2]
        ci = seq.clustid
        write(f,"$ci\t$(split(h[2:end])[1])\t")
        for p in 1:glob.wlength
            if seq.revcomp
                n0 = glob.wlength+1-p
            else
                n0=p
            end
            n0 += seq.shift
            if n0<1 || n0>seq.lseq
                write(f,"N")
            elseif seq.revcomp
                write(f,rcdict[s[n0]])
            else
                write(f,s[n0])
            end
        end
        if seq.revcomp
            write(f,"\t(-):")
        else
            write(f,"\t(+):")
        end
        if seq.shift>0
            write(f,"+")
        end
        write(f,"$(seq.shift):")
        write(f,string(seqLikelihood(seq,clusters[seq.clustid],glob)-seq.lseq*log(0.25)))
        write(f,"\n")
    end
end


function wmFromBasecounts(basecounts::Array{Float64,2}) # pseudocount is included in basecounts!
    return [[x/sum(view(basecounts,n,:)) for x in view(basecounts,n,:)] for n in 1:size(basecounts)[1]]
end

#= clustering clusters:

hierarchical clustering
* find closest nodes measured by a modified cluster_likelihood
  where a cluster-of-clusters is treated as one cluster
* while n_clusters > maxclusters, merge_clusters into single
  clusters while joining neighbours
* for printing, order each pair of children by the one most similar to previ  ously printed cluster 

=#
#FIXME do something about pscount or get rid of it!
mutable struct CNode
    cl::SeqCluster
    wm::Array{Array{Float64,1},1}
    basecounts::Array{Float64,2}
    nseqs::Int64
    isleaf::Bool
    child1::CNode
    child2::CNode
    CNode() = new()
    CNode(cl) = new(cl,wmFromBasecounts(cl.basecounts),copy(cl.basecounts),cl.nseqs,true)
    CNode(child1,child2,pscount::Float64) =
        new(nullCluster,(child1.wm*child1.nseqs + child2.wm*child2.nseqs)
            /(child1.nseqs+child2.nseqs),
            copy(child1.basecounts)+copy(child2.basecounts) .- pscount,
            child1.cl.nseqs+child2.cl.nseqs,false,child1,child2)
end

nullCnode = CNode(nullCluster)

#Jensen-Shannon divergence
function js_div(wv1::Array{Float64,1},wv2::Array{Float64,1})
    m = 0.5*(wv1+wv2)
    return 0.5*sum([wv1[n]*log(wv1[n]/m[n]) + wv2[n]*log(wv2[n]/m[n])
                    for n in 1:4])
end                


function js_div(wm1::Array{Array{Float64,1},1},
                wm2::Array{Array{Float64,1},1})
    return sum([js_div(wm1[n],wm2[n]) for n in 1:length(wm1)])
end


function js_div(c1::CNode,c2::CNode)
    return js_div(c1.wm,c2.wm)
end

#equivalent of clusterLikelihood, for cnodes
function cnodeLikelihood(glob::Params,nc1::CNode,nc2::CNode=nullCnode,shift::Int64=0,revcomp::Bool=false)
    bc = copy(nc1.basecounts)
    bc2 = copy(nc2.basecounts)
    lseq = glob.wlength
    if shift==0 && revcomp==0
        if nc2 != nullCnode
            bc = bc + nc2.basecounts .- glob.pscount
        end
    else
        # divide the shift equally?
        shift1 = -div(shift,2)
        shift2 = shift+shift1
        #shift1 = 0
        #shift2 = shift
        #println(shift1," ",shift2)
        for n0 in 1:lseq
            n1 = n0+shift1
            if revcomp
                n2 = lseq+1-n0-shift2
            else
                n2 = n0+shift2
            end
            #n2 += shift2
            for m in 1:4
                if n1 <= 0 || n1 > lseq
                    bc[n0,m] = nc1.nseqs*0.25+glob.pscount
                else
                    bc[n0,m] = nc1.basecounts[n1,m]
                end
                if revcomp
                    m2 = 5-m
                else
                    m2 = m
                end
                if n2 <= 0 || n2 > lseq 
                    bc2[n0,m2] = nc2.nseqs*0.25+glob.pscount
                else
                    bc2[n0,m2] = nc2.basecounts[n2,m]
                end
            end
        end
        #println(bc[40:60,:])
        #println(bc2[40:60,:])
        bc += bc2
        bc = bc .- glob.pscount
    end
    ntot = nc1.nseqs+nc2.nseqs+4.0*glob.pscount
    loglik = 0.0
    p4 = glob.pscount*4.0
    lpfac = lgamma(p4)-4*lgamma(glob.pscount)-lgamma(ntot)
    loglik = lseq*lpfac
    for n in 1:lseq
        for m in 1:4
            @inbounds loglik += lgamma(bc[n,m])
        end
    end
    return loglik
end

function cnodeLLR(nc1::CNode,nc2::CNode,glob::Params)
    return cnodeLikelihood(glob,nc1,nullCnode) +
        cnodeLikelihood(glob,nc2,nullCnode) -
        cnodeLikelihood(glob,nc1,nc2)
end

function cnodeLLRAllShifts(nc1::CNode,nc2::CNode,glob::Params)
    llr0 = cnodeLikelihood(glob,nc1,nullCnode)+cnodeLikelihood(glob,nc2,nullCnode)
    llrs = Tuple{Float64,Int64,Bool}[]
    if glob.use_shift
        shiftrange = -div(glob.wlength,2):div(glob.wlength,2)
    else
        shiftrange = Int64[0]
    end
    for shift in shiftrange
        for revcomp in [true,false]
            push!(llrs,(llr0-cnodeLikelihood(glob,nc1,nc2,shift,revcomp),shift,revcomp))
        end
    end
    return minimum(llrs)
end

function eucdist(node1::CNode,node2::CNode)
    return sum([sum([x^2 for x in node1.wm[n]-node2.wm[n]]) for n in 1:length(node1.wm)])^0.5
end

# combine the two clusters into one (the first)


function shiftCNode!(node1::CNode,shift::Int64,revcomp::Bool,glob::Params)
    if node1.isleaf
        scnew = setupNullCluster()
        for n in 1:node1.cl.nseqs
            seq0 = popCluster!(node1.cl,glob)
            if revcomp
                seq0.revcomp = !seq0.revcomp
            end
            if seq0.revcomp
                seq0.shift -= shift
            else
                seq0.shift += shift
            end
            pushCluster!(scnew,seq0,glob)
        end
        return CNode(scnew)
    else
        ch1 = shiftCNode!(node1.child1,shift,revcomp,glob)
        ch2 = shiftCNode!(node1.child2,shift,revcomp,glob)
        return CNode(ch1,ch2,glob.pscount)
    end
end

function mergeClustersWithShift!(sc1::SeqCluster,sc2::SeqCluster,shift::Int64,
                                 revcomp::Bool,glob::Params)

    scnew = setupNullCluster()
    shift1 = -div(shift,2)
    shift2 = shift+shift1
    for n in 1:sc1.nseqs
        seq0 = spliceCluster!(sc1,1,glob)
        if seq0.revcomp
            seq0.shift -= shift1
        else
            seq0.shift += shift1
        end
        pushCluster!(scnew,seq0,glob)
    end
    for n in 1:sc2.nseqs
        seq0 = popCluster!(sc2,glob)
        if revcomp
            seq0.revcomp = !seq0.revcomp
        end
        if seq0.revcomp
            seq0.shift -= shift2
        else
            seq0.shift += shift2
        end
        pushCluster!(scnew,seq0,glob)
    end
    sc2.allowsplit=true
    sc1.allowsplit=true
    #shiftOneCluster!(sc1)
    return scnew
end


#TODO: do a cnodeLLRAllShifts; push best one including shift/rc
# onto distlist;

function clusterClusters(clusterlist::Array{SeqCluster,1},glob::Params)
    nodelist = CNode[CNode(c) for c in clusterlist]
    nleaves = length(nodelist)
    while length(nodelist)>1
        distlist = Tuple{Tuple{Float64,Int64,Bool},Int64,Int64}[]
        for n in 1:length(nodelist)-1
            for m in n+1:length(nodelist)
                #push!(distlist,(js_div(nodelist[n],nodelist[m]),n,m))
                try
                    push!(distlist,(cnodeLLRAllShifts(nodelist[n],nodelist[m],glob),n,m))
                catch
                    println("$n $m")
                    println("$(cnodeLLRAllShifts(nodelist[n],nodelist[m],glob))")
                    println(nodelist[n].cl.basecounts)
                    println(nodelist[m].cl.basecounts)
                end
                #push!(distlist,(eucdist(nodelist[n],nodelist[m]),n,m))
            end
        end
        (mdist,shift,revcomp),nmin,mmin = minimum(distlist)
        if glob.debug
            println("$mdist $shift $revcomp $nmin $mmin")
        end
        node2 = splice!(nodelist,mmin)
        node1 = splice!(nodelist,nmin)
        mdistthres = 1.0*clThres(glob,node1.cl.nseqs,node2.cl.nseqs)
        if (node1.isleaf && node2.isleaf) &&
            (mdist < mdistthres || (glob.maxnclust >0 && nleaves > glob.maxnclust))
            if glob.debug
                println("$mdist $(length(nodelist)) $(glob.maxnclust)")
            end
            cl1 = node1.cl
            cl2 = node2.cl
            #backupShiftRevcomp!(cl1)
            #backupShiftRevcomp!(cl2)
            cl1=mergeClustersWithShift!(cl1,cl2,shift,revcomp,glob)
            newnode = CNode(cl1)
            nleaves -= 1
        else
            # shift clusters in node1 and node2 appropriately
            shift1 = -div(shift,2)
            shift2 = shift+shift1
            node1 = shiftCNode!(node1,shift1,false,glob)
            node2 = shiftCNode!(node2,shift2,revcomp,glob)
            newnode = CNode(node1,node2,glob.pscount)
        end
        push!(nodelist,newnode)        
    end
    return nodelist[1]
end


function addLeaves(node::CNode,leaflist::Array{SeqCluster,1},glob::Params)
    if node.isleaf
        push!(leaflist,node.cl)
        return leaflist
    else
        child1 = node.child1
        child2 = node.child2
        if length(leaflist)>0 && cnodeLLR(leaflist[end],child1,glob)>cnodeLLR(leaflist[end],child2,glob)
        #if length(leaflist)>0 && eucdist(leaflist[end],child1)>eucdist(leaflist[end],child2)
            child1,child2 = child2,child1
        end
        append!(leaflist,addLeaves(child1,SeqCluster[],glob))
        append!(leaflist,addLeaves(child2,SeqCluster[],glob))
    end
    return leaflist
end


function orderClusters(cl::Array{SeqCluster,1},glob::Params)
    rootnode = clusterClusters(cl,glob)
    return addLeaves(rootnode,SeqCluster[],glob)
end



                                 
