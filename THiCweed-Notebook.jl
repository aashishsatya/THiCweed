include("thicweed_lib.jl")

filename = "synth_300.fa"

glob = Params(0.0, 0, 0.0, 0.0, 0, 0, 0,true,true,true,false,0.2)    
glob.randlim = 0.2
glob.minclustsize= 20
glob.infthres = -1.0
glob.pscount = 0.5
glob.maxnclust = 25
glob.maxsplitclusters = 100
glob.use_revcomp = true
glob.use_shift = true
glob.debug = false
glob.wlength = -1
glob.hardmask = false
glob.clthres = 0.4

fseqs = readfasta(filename,glob)

seqs = [s::String for (h,s) in fseqs]::Array{String,1};

if glob.wlength < 0
    if glob.use_shift
        ls = Int64[length(s) for s in seqs]
        sort!(ls)
        glob.wlength = div(ls[div(length(ls),2)],3)::Int64
    else
        glob.wlength = maximum([length(s) for s in seqs])
    end
end

# clock the time required
#@prun clusters = clusterSequences(seqs, glob)
