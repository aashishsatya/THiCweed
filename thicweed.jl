#!/usr/bin/env julia
# Version 1.0, 2018-09-19
# Written by Rahul Siddharthan <rsidd@imsc.res.in>, (c) 2017-18.
# Licence: 2-clause BSD-like.  See http://www.imsc.res.in/~rsidd/thicweed


include("thicweed_lib.jl")

using Random;
using SpecialFunctions;

# parse command line arguments    
using ArgParse;
function parse_cmdline()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--length","-l"
        help = "length of window size to be aligned within longer sequence (default: 2/3 median input sequence length)"
        arg_type = Int64
        default = -1
        "--shift", "-S"
        help = "do NOT consider shifted versions of sequences (default: consider shifts; if disabled also disables revcomp)"
        action = :store_false
        "--debug", "-d"
        help = "print verbose debugging message"
        action = :store_true
        "--hardmask","-H"
        help = "hardmask: treat lowercase input letters as N"
        action = :store_true
        "--revcomp","-R"
        help = "do NOT consider reverse complements of input sequences (default: consider them if using shifts, not otherwise)"
        action = :store_false
        "--randlim","-r"
        help = "rand-index threshold (reject splits that are less consistent than this) (default: 0.0=disabled)"
        arg_type = Float64
        default = 0.4
        "--minclustsize","-m"
            help = "minimum cluster size (default 20)"
            arg_type = Int64
        default = 20
        "--infthres","-i"
        help = "consider only positions with at least this information content when splitting (0.0 = consider all positions; default / negative values: automatic choice per cluster)"
        arg_type = Float64
        default = -1.0
        "--maxnclust","-N"
        help="maximum number of output clusters (0 = no limit)"
        arg_type = Int64
        default = 15
        "--maxsplitclusters","-M"
        help = "maximum number of clusters to internally split into (0 = no limit)"
        arg_type = Int64
        default = 30
        "--out_prefix","-o"
        help = "prefix of output files (output_wms.tr and output_archs.txt will be generated)"
        arg_type = String
        default = ""
        "--clthres","-T"
        help = "parameter for cluster split significance (0 = disabled)"
        arg_type = Float64
        default = 0.2
        "--seed","-s"
        help = "set global random number generator seed (single integer)"
        arg_type = UInt32
        default = convert(UInt32,0x0)
        "filename"
            help = "sequences to be clustered, in fasta format"
            required = true
    end
    return parse_args(s)
end


function main()
    filename=""
    glob = Params(0.0, 0, 0.0, 0.0, 0, 0, 0,true,true,true,false,0.2)    
    glob.randlim = 0.2
    glob.minclustsize= 20
    glob.infthres = -1.0
    glob.pscount = 0.5
    glob.maxnclust = 25
    glob.maxsplitclusters = 100
    glob.use_revcomp = true
    glob.use_shift = true
    glob.debug = false
    glob.wlength = -1
    glob.hardmask = false
    glob.clthres = 0.4
    
    out_prefix = ""

    parsed_args = parse_cmdline()
    for (arg,val) in parsed_args
        if arg=="length"
            glob.wlength = val
        elseif arg=="shift"
            glob.use_shift = val
        elseif arg=="revcomp"
            glob.use_revcomp = val
        elseif arg=="debug"
            glob.debug = val
        elseif arg=="minclustsize"
            glob.minclustsize=val
        elseif arg=="randlim"
            glob.randlim=val
        elseif arg=="filename"
            filename = val
        elseif arg=="infthres"
            glob.infthres = val
        elseif arg=="maxnclust"
            glob.maxnclust = val
        elseif arg=="maxsplitclusters"
            glob.maxsplitclusters = val
        elseif arg=="out_prefix"
            out_prefix = val
        elseif arg=="hardmask"
            glob.hardmask=val
        elseif arg=="clthres"
            glob.clthres = val
        elseif arg=="seed"
            if val>0x0
                srand([convert(UInt32,val)])
            end
        end
    end

    if glob.use_shift == false
        glob.use_revcomp = false
    end

    fseqs = readfasta(filename,glob)
    
    seqs = [s::String for (h,s) in fseqs]::Array{String,1};
    if glob.wlength < 0
        if glob.use_shift
            ls = Int64[length(s) for s in seqs]
            sort!(ls)
            glob.wlength = div(ls[div(length(ls),2)],3)::Int64
        else
            glob.wlength = maximum([length(s) for s in seqs])
        end
    end

    if out_prefix == ""
        out_prefix = filename
    end

    print("""
$(length(seqs)) sequences read from $(filename). 

Input parameters:
Window length                       : $(glob.wlength)
Use shifts                          : $(glob.use_shift)
Rand limit                          : $(glob.randlim)
Dinuc threshold                     : $(glob.clthres)
Use reverse complement              : $(glob.use_revcomp)
Minimum cluster size                : $(glob.minclustsize)
Maximum number of output clusters   : $(glob.maxnclust)
Informative threshold               : $(glob.infthres) (-1.0 means automatic)
Hardmask                            : $(glob.hardmask)
Maximum num of clusters when running: $(glob.maxsplitclusters)
Output prefix                       : $(out_prefix)
RNG seed                            : $(Random.GLOBAL_RNG.seed)
""")

    clusters_raw=clusterSequences(seqs,glob)

    if glob.maxnclust==0 || length(clusters_raw)<=glob.maxnclust
        println("\nReordering clusters...")
    else
        println("\nReordering and merging clusters...")
    end
    clusters_raw1 = orderClusters(clusters_raw,glob)
    clusters = orderClusters(clusters_raw1,glob)

    #write architectures to file filename_archs.txt
    print("Writing ",length(clusters)," architectures to ",out_prefix,"_archs.txt ...\n")
    writearchs(string(out_prefix,"_archs.txt"),clusters,fseqs,glob)

    #write weight matrices to file filename_wms.tr
    println("Writing ",length(clusters)," matrices to ",out_prefix,"_wms.tr ... ")
    push!(clusters,setupSeqCluster(seqs,glob))
    writetransfac(string(out_prefix,"_wms.tr"),clusters,glob);
    println("Done.")
end

main()



